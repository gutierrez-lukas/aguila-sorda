var ping = require('ping');
var exec = require('child_process').exec;

var port = process.env.npm_config_myPort || 443;
var host = process.env.npm_config_myHost || 'AR-IT04325';

ping.promise.probe(host)
	.then(function(res){
		var ipChecked = res.numeric_host + " " + port;
		console.log(ipChecked);
		exec("perl aguilaSorda.pl " + ipChecked, function(err, stdout, stderr) {
			console.log("exec aguila sorda");
			if (err) console.log(err);
			
			if (stdout) return stdout;
			if (stderr) return stderr;
		});
	});